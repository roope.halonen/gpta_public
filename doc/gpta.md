# GPTA 2.0 List of commands

 Author : Paolo Raiteri - Curtin University
 Email  : p.raiteri@curtin.edu.au

## Philosophy
A part from few commands that affect the general behaviour of GPTA, all the other commands are executed in order, from left to right, for each of the frames contained in the trajectory.  Therefore, the command for reading the input file(s) usually comes first, while the output commands normally comes last.
Although I tried to make sure that each commands does not leave behind any dirt, it is possible that some particular sequences of commands are not compatible.
If you come across any of such a situations, the best approach is to create several intermediate files and break your workflow in individual steps and where one or few operations are done in each step. You could also send me the input file and the commands that you are trying to run... and wait for a patch.

As a general rule `-` precedes a command  while a `+` precedes a flag of the command that has just been invoked.

In the following I will try to stick to this convention:
`()` indicates a mandatory field
`{}` indicates an optional field
The content of the brackets will then indicate the type of variable that is expected:
`i` or `n` :: integer number
`r`        :: real number
`str`      :: a string
`l`        :: atom label

Most of the commands can be used multiple times in the same run.

These flags are available for all commands:
```
+o (filename)         -> override the default output filename
+xo (flag) (filename) -> output name for eXtra features
+stdout               -> writes output, if any, on standard output
+stderr               -> writes output, if any, on standard error
+debug                -> turn on debugging for the command
+frac                 -> writes fractional coordinates instead of cartesian
```

## TO ADD
1. library file

## Screen output commands
### -quiet
removes all outputs from the screen.
<br>

### -v
enables verbose screen output.
<br>

### -debug {i}
enables debugging screen output.
There are two level of verbosity `{i=1}` or `{i=2}`
<br>

### -h
writes the list of all available commands.
<br>

### -help {str} ...
writes a brief description of the command(s) specified by `{str} ...`. 
If `{cmd}` is not present then a description of all commands is written.
##### Example:
```text
gpta.x -help gofr
```
<br>

## Frame selection commands
This commands can be placed anywhere on the command line without altering their effect.
### -frame (iframe)
selects one single frame for processing
<br>

### -frames (iframe) {jframe}
selects an interval of frames for processing, boundaries included.
If `jframe` is not specified the analysis is performed from `iframe` till the last frame.
<br>

### -skip {n}
selects every n-th frame for processing, starting from the first one.
The first frame to be process can be changed with the command `-frames (iframe)`
<br>

### -last
selects only the last frame of the trajectory for processing.
<br>

## Default change:
### -safe_pbc
changes the pbc calculation to the 'safe mode' where the closet distance is found by looping on the 26 neighbours cells. 
It is useful for simulations with very skewedi cells but it makes the calculations of distances very slow.
<br>

### -sbond
defines the cutoff distance for special bonds, e.g. to define clusters of molecules.
Up to 6 atomic pairs can be define with one command.
##### Required flags:
```text
+s l1 l2 ... -> defines the two atomic species which have a special bond length
+dr r1 ...   -> defines the bond length
```
<br>

### -random (iseed)
defines the seed to initialise the random number generator.
<br>

## Input/Output commands
### -i (filename1) {filename2} ...
defines the input file names. 
Multiple files are allowed in one command and will be concatenated.
The filetype is recognised from the extension or it can be attached to the command, e.g. ```-ipdb```.
Recognised types: xyz, pdb, dlp, lmp, ltr, gin, gro, gau(ssian), nwc, arc, mol2, fdf, rst, dcd
##### Optional flags:
```text
+frac -	> input is in fractional coordinates (must follow the name of the file it refers to)
```
<br>

##### Example:
```text
gpta.x -i solute.pdb +frac trajectory.dcd
gpta.x -ipdb solute.PDB
```
<br>

### -o (filename) / -o(filetype) (filename) 
defines the output filename. 
The output type is defined by the extension of the output filename or by the string attached to the command.  
For example `-o coord.xyz` and `-oxyz` will produce an XYZ file regardless of the extension of the output file.  
In the second case the file extension is ignored and the filename is optional. 
If the filename is omitted a file with the same root as the input and the new filetype extension will be created. 
##### Optional flags:
```text
+append -> appends the output coordinates to an existing file
```
<br>

### -lmp
write a LAMMPS input file with the molecular connectivity.
##### Required flags:
```text
+f (FF file)   -> LAMMPS forcefield with GPTA mark-ups
```
<br>

##### Optional flags:
```text
+o {coord.lmp} -> writes the LAMMPS coordinates files
+nobond        -> ignores missing FF bonds parameters
+noangle       -> ignores missing FF angles parameters
+nodihe        -> ignores missing FF dihedral parameters
+noimpr        -> ignores missing FF improper parameters
+evb           -> writes the RAPTOR evb.top file | requires `in.evb` and `evb.par` 
```
<br>

### -merge
merges the coordinates of a file with the loaded one.
##### Required flags:
```text
+f (filename)
```
<br>

## Atoms selections
### -select 
selects atoms according to different functions for later use.
##### Selection types:
```text
+s (l1) ...           -> atoms labels
+i (indx1) ...        -> atoms indices
+l (lower) (upper)    -> atoms in slabs between `lower` and `upper`
  +x / +y / +z        -> normal to the slab

+sphere               -> atoms is a sphere
  +r (r)                -> radius of the sphere
  +pos (p1) (p2) (p3)   -> centre of the sphere

+box                  -> atoms is a box
  +x0 (x,y,z)           -> origin of the box
  +mtx (A) (B) (Cz)     -> box vectors (9 numbers)

+random               -> random selection of atoms or molecules from the selection
  +n                    -> # of atoms or molecules randomly selected 
```

##### Optional flags:
```text
+not   -> inverts the selection
+mol   -> selects entire molecules when at least one atoms is selected
+whole -> selects only those molecules that have all the atoms selected
```
<br>

## Topology commands
### -neigh
enforces the calculation of the neighbours list.
Every command that requires the neighbours list allows to change the cutoff distance with 
the +dn flag, so this command should be redundant if not for debugging.
##### Optional flags:
```text
+dn (r)     -> defines the distance for the neighbours list
+update (i) -> defines the update frequency of the neighbours list
+s (lab)... -> defines which species are included in the neighbours list
+verlet     -> forces GPTA to use the verlet algorithm instead of the linked cell
```
<br>

### -top
calculates the system topology.
Two atoms are considered bonded if their distance is smaller than the sum of their vdw radii multiplied by a scaling factor.
##### Optional flags:
```text
+r (1.15)                  -> scaling factor 
+dn (3.0)                  -> distance for the neighbours list
+n (100)                   -> maximum molecule size
+full                      -> writes the full topology (bonds, angles, dihedrals and impropers)
+rebuild                   -> rebuilds the molecules if they broken across the boundaries
+ion (lab)...              -> forces species to be atomic ions and have no bonds
+oh                        -> guesses hydroxide molecules and renames atoms accordingly
+ho2                       -> guesses water molecules and renames atoms accordingly
+h3o                       -> guesses hydronium molecules and renames atoms accordingly
+co3                       -> guesses carbonate molecules and renames atoms accordingly
+hco3                      -> guesses bicarbonate molecules and renames atoms accordingly
+so4                       -> guesses sulfate molecules and renames atoms accordingly
+oxa                       -> guesses oxalate molecules and renames atoms accordingly
```

##### Example:
```text
gpta.x -i solute.pdb solute.0.dcd  \
       -top +dn 4 +rebuild -sbond Ca C4 4.0  Ca C5 4.0 +rebuild +n 1000  \
       -pbc -o new.dcd
```
<br>

### -system (mol_name) ... / -system (nmol) (nat_mol1) ... (l1_mol1) ...  
allows for defining the molecules by looking at the atoms names
It assumes that the molecules are listed sequentially with the atoms always in the order specified on the command line.
The molecules can be specified by giving the number of molecules, atoms in each molecule and the sequennce of labels.
Alternatively the list of molecules names can be used, if they are included in the GPTA library.
##### Optional flags:
```text
+rebuild -> rebuilds the molecules if they broken across the boundaries
```

##### Example: calcium carbonate in water
```text
gpta.x -i coord.pdb -system Ca CO3 H2O
gpta.x -i coord.pdb -system 3  1 4 3  Ca  C4 O4 O4 O4  O2 H2 H2
```
<br>

### -reorder *\*\*\*deprecated\*\*\**
<br>

### -zmatrix
calculates the z-matrix
<br>

## System manipulation commands
### -pbc 
wraps the atoms/molecules inside the unit cell.
If the topology of the system is known the molecules are not broken across the boundary.
##### Optional flags:
```text
+nint -> molecules are wrapped in the interval [-0.5:0.5[
```
<br>

### -rename 
allows for changing the labels of the atoms.
##### Required flags (at least one):
```text
+s (l1) (l2) {l3} {l4} ... -> changes the label from l1 to l2, l3 to l4...
+i (i1) +s (l1)            -> renames all atoms with indices i1 to l1 (poorly tested)
```
<br>

### -charge 
defines the atoms charges based on their labels.
##### Required flags:
```text
+q (l1) (q1) {l2} {q2} ... -> sets the charge of atoms l1 to q1, l2 to q2 ....
```
<br>

### -remove
removes atoms. This can be used to speed up the neighbours list or remove the solvent from a configuration. 
##### Required flags (at least one):
```text
+s (l1) {l2} ... -> removes atoms labeled l1, l2 ...
+i (i1) {i2} ... -> removes atoms with indices i1, i2...
+sel (s1) {s2} ... -> removes atoms from selections s1, s2...
```
<br>

### -create
creates species into the system.
It can be used without any input file if a box is defined with `-cell`.
##### Required flags (at least one):
```text
+s {s1} {s2}...      -> creates random molecules of `species`
+f {filename}        -> creates random copies of the molecule in the file 
+n {n1} {n2}...      -> sets the number of each species to be created
```

##### Optional flags:
```text
+r (r)               -> sets the minimum distance for the overlaps
+centre              -> the molecule will be placed at the centre of the cell (works only for one solute added)
+origin              -> the molecule will be placed at the origin of the axes (works only for one solute added)
+pos (x,y,z)         -> specifies where the molecule will be placed (works only for one solute added)
+before              -> adds the new atoms before the old ones (default)
+after               -> adds the new atoms after the old ones
```
<br>

### -library
adds a molecule to the library (max 100 atoms)
##### Required flags:
```text
+f (filename) -> filename with the molecule to be added into the library.
```
<br>

### -align
aligns selected a molecule/fragment with respect to its position in the first frame.
##### Required flags:
```text
+s (l1) {l2} ...   -> aligns atoms labeled l1, l2 ...
+i (i1) {i2} ...   -> aligns atoms with indices i1, i2...
+sel (s1) {s2} ... -> aligns atoms from selections s1, s2...
```
<br>

### -unwrap
unwraps the atoms with respect to their position in the previous frame of the trajectory
It is useful for studying the diffusion of the species.
<br>

### -fixcom
moves the centre of mass of the system to a chosen location in the cell.
A subset of the system to be used for the centering can be specified with the flag +s
Note that in a periodic system the position of the centre of mass is ill-defined and strange behaviour can be observed.
##### Required flags (at least one):
```text
+s (l1) ...   -> uses the atoms labeled l1... for the definition of the c.o.m.
+i (i1) ...   -> uses the atoms with indices i1... for the definition of the c.o.m.
+sel (s1) ... -> uses the atoms from selections s1... for the definition of the c.o.m.
```
##### Optional flags:
```text
+centre           -> the c.o.m. will be placed at the centre of the cell
+origin           -> the c.o.m. will be placed at the origin of the axes
+pos (x) (y) (z)  -> the c.o.m. will be placed at position `(x,y,z)`
+fix              -> the c.o.m. will be kept in the same position as in frame 1 
```
##### Example: move a calcium carbonate nanoparticle to the centre of the cell
```text
-fixcom +s Ca C4 O4 +centre
```
<br>

### -noclash
removes clashes between molecules. The molecules are processed in order and the molecule with the higher index is removed.
##### Optional flags:
```text
+r (r)   -> sets the minimum distance for considering the molecules as non-overlapping
+nosame  -> does not remove molecules of the same type
+reverse -> removes molecules with the lower indices
```
<br>

### -cell 
defines the simulation box overriding any previous values.
##### Required flags:
```
+vec (l)                                            -> defines a cubic cell
+vec (a) (b) (c)                                    -> defines an orthorombic cell
+vec (a) (b) (c) (alpha) (beta) (gamma)             -> defines a triclinic cell
+mtx (ax) (ay) (az)  (bx) (by) (bz)  (cx) (cy) (cz) -> defines a triclinic cell  
```
##### Optional flags:
```text
+straight -> tries to straighten up the cell
+scale    -> scales the atomic coordinates if a cell was already defined
```
<br>

### -repl (...nargs...)
replicates the system in all the crystallographic directions.  It can be used either with 3 arguments or with 6 arguments.
If the topology is known the replicas are built molecule by molecule instead of atom by atom.
##### Example 1: produces a system 2 times bigger in each direction
```text
gpta.x -i coord.pdb -repl 2 2 2 -> 
```
##### Example 2: produces a system 4 times bigger in each direction
```text
gpta.x -i coord.pdb -top -repl -1 2  -1 2  -1 2 
```
<br>

### -scale *\*\*\*deprecated\*\*\**
re-scales the system cell. 
The rescaling can be defined either as a multiplicative factor for each crystallographic vector or as a completely new matrix.
This command will be removed as is included in the more powerful `-cell` command.
##### Optional flags:
```text
+mtx (Ax) (Ay) (Az)  (Bx) (By) (Bz)  (Cx) (Cy) (Cz) 
+vec (Sa) (Sb) (Sc)
```
<br>

### -transf
applies a transformation to the atomic coordinate of the whole system.
##### Required flags (at least one):
```text
+move                 -> moves the atoms 
  +dr (r1) (r2) (r3)    -> by a vector (r1,r2,r3)

+rotate               -> rotates the atoms 
  +vec (v1) (v2) (v3)   -> around the vector (v1,v2,v3) 
  +pos (p1) (p2) (p3)   -> with origin in (p1,p2,p3)
  +r (phi)              -> by an angle phi clockwise

+mirror               -> reflects the atoms
+vec (v1) (v2) (v3)     -> normal to the mirror plane
+pos (p1) (p2) (p3)     -> mirror position
```
<br>

### -surface
generates a set of surfaces normal to a chosen vector. All possible atomic cuts are written in the output file.
##### Required flag:
```text
+vec (i1) (i2) (i3) -> miller indices of the desired surface
```
<br>

### -match
matches the molecules of one species with a template structure and reorder the atoms.
To use this command molecules must be already known and not fragmented across the boundary.
##### Notes:
Before this command you must use `-topology +group +rebuild`, `-system +rebuild` or `-reorder +rebuild`.
The topology should also be recalculated afterwards.
The alignment of the 
##### Required flags (either +f or +s):
```text
+sel          -> selects the molecule ID to be matched with the template
+f (filename) -> reads the template molecule from a file
+s (species)  -> uses a molecule from the internal library as a template
```
##### Optional flags:
```text
+noh          -> exclude hydrogens in the alignment step
+nomass       -> uses the geometric centre for the molecules for the alignment
+replace      -> substitutes the molecules with the template
+charges      -> substitutes the atomic charges with the template ones
+rename       -> renames the atoms according to the template molecule
+labels       -> atoms in the system and ref molecules have the same labels
```
<br>

### -molsubs
replaces selected molecules with a different one.
If the new molecule is of same species the `-match` command might be a better option.
The geometry of the new molecule must be specified in a file (default name: molinfo.inp).
##### sample file for the reference molecule:
```text
4
 carbonate
 C4     0.000000     0.000000     0.000000   1.123285   1
 O4     1.270000     0.000000     0.000000  -1.041095   2
 O4    -0.635000     1.099852     0.000000  -1.041095   3
 O4    -0.635000    -1.099852     0.000000  -1.041095   4
```

This is a standard XYZ file with one extra column.
The first line contains the number of atoms in the molecule followed by a comment line.
Then there is one line per atoms, containing the atom label,  the x, y and z coordinates, the charge i
and the ID of the reference atom in the old molecule.
The column with the atomic charges is optional
##### Notes:
Before this command you must use `-topology +group +rebuild`, `-system +rebuild` or `-reorder +rebuild`.
The topology should also be recalculated afterwards.
##### Optional flags:
```text
sel (i1) ...     -> selects the molecule ID(s) to be matched with the template
+f (molinfo.inp) -> overrides the default input filename
```
<br>

## Properties commands
### -print
writes out some properties of the system
##### Required flags (at least one):
```text
+vol    -> cell volume
+hmat   -> cell matrix
+a      -> a vector of the simulation cell
+b      -> b vector of the simulation cell
+c      -> c vector of the simulation cell
+cell   -> length and angles of the simulation cell
+dens   -> density
+dist   -> distance between two groups of atoms
  +i (i1) ... +i (i2) ..      -> atoms defined by indices
  +s (l1) ... +s (l2) ...     -> atoms defined by labels
  +sel (i1) ... +sel (i2) ... -> atoms defined by selections
+xcom   -> writes the centre of mass of the molecules (requires -top or -system)
```
##### Optional flags:
```text
+o      -> (filename) overrides the default output filename
+screen -> writes the output on standard output 
+stderr -> writes the output on standard error
```
<br>

### -pos
writes out the position or the centre of mass of selected atoms in file. 
The selection can be made by the atomic indices, labels or a previous selection. 
Any format for the coordinates that is known to GPTA can be used for the output file
##### Required flags (at least one):
```text
+i (i1) {i2} ... -> selects atoms with indices i1, i2...
+s (l1) {l2} ... -> selects atoms labeled l1, l2 ...
+sel (s1) {s2} ... -> selects atoms from selections s1, s2...
```
##### Optional flags:
```text
+traj writes all the coordinates in the output file instead of the c.o.m position
+o (filename) overrides the default output filename
+screen writes the output on standard output (unit=6)
+stderr writes the output on standard error (unit=0)
```
<br>

### -average
calculates some averages properties along an MD trajectory.
##### Required flags (pick least one):
```text
+cell/+box -> averages the simulation cell
+atm       -> averages the positions of the atoms
+sys       -> averages both the simulation cell and the positions of the atoms
+stdev     -> calculated the stdev for the postitions (writes stdev_pos.dat)
```
##### Optional flags:
```text
+o (filename) -> overrides the default output filename
+screen       -> writes the output on standard output (unit=6)
+stderr       -> writes the output on standard error (unit=0)
```
<br>

### -gofr
calculates the pair distribution function (PDF).
##### Optional flags:
```text
+i (i1) {i2}   -> calculates the PDF of species i2 with respect to species i1
+s (l1) {l2}   -> calculates the PDF of species l2 with respect to species l1
+sel (s1) {s2} -> calculates the PDF of species s2 with respect to species s1
+navg          -> disables averaging along a trajectory
+r (8.0)       -> defines the cutoff in the calculation of the PDF
+n (100)       -> overrides the default number of bins
+o (filename)  -> overrides the default output filename
+screen        -> writes the output on standard output (unit=6)
+stderr        -> writes the output on standard error (unit=0)
```
<br>

### -angdist
calculates the angular distribution function.
##### Optional flags:
```text
+s (l1) (l2) (l3)   -> calculates the angle as (l1)--(l2)--(l3)
+i (i1) (i2) (i3)   -> calculates the angle as (i1)--(i2)--(i3)
+sel (s1) (s2) (s3) -> calculates the angle as (s1)--(s2)--(s3)
+navg               -> disables averaging along a trajectory
+r (2.0)            -> defines the cutoff for the calculation of the coordination number
+n (100)            -> overrides the default number of bins
+o (filename)       -> overrides the default output filename
+screen             -> writes the output on standard output (unit=6)
+stderr             -> writes the output on standard error (unit=0)
```
<br>

### -coordn
calculates the coordination number around selected species.
##### Optional flags:
```text
+i (i1) {i2}       -> calculates the coord of species i2 with respect to species i1
+s (l1) {l2}       -> calculates the coord of species l2 with respect to species l1
+sel (sel1) {sel2} -> calculates the coord of species s2 with respect to species s1
+navg              -> disables averaging along a trajectory
+r (3.0)           -> defines the cutoff for the calculation of the coordination number
+atomic            -> calculates the coordination number per atom
+vmd               -> calculates the coordination number per atom + vmd friendly output
+dist              -> calculates the distribution of coordination numbers
+n (100)           -> overrides the default number of bins
+o (filename)      -> overrides the default output filename
+screen            -> writes the output on standard output (unit=6)
+stderr            -> writes the output on standard error (unit=0)
```
<br>

### -res_time
calculates the residence time of the oxygen of water molecules (O2) in the first solvation shell of selected species.
##### Optional flags:
```text
+i (i1) {i2}       -> selects the coordination shell of (i1)
+s (l1) {l2}       -> selects the coordination shell of (l1)
+sel (sel1) {sel2} -> selects the coordination shell of (sel1)
+r (3.0)           -> defines the cutoff for the definition of the coordination shell
+dt (time)         -> defines the time interval between frames in ps (??)
+o (filename)      -> overrides the default output filename
+screen            -> writes the output on standard output (unit=6)
+stderr            -> writes the output on standard error (unit=0)
+n (nframes)       -> sets the threshold for deciding whether the water 
                      is in the solvation shell or it has left it permanently
```
<br>

### -msd
calculates the mean square displacement of the selected species.
##### Note:
In order to ensure a meaningful calculation the -unwrap command is enforced beforehand.
##### Required flags (pick one):
```text
+s (l1) {l2} ...       -> selects (i1) ... for the calculation
+i (l1) {l2} ...       -> selects (l1) ... for the calculation
+sel (sel1) {sel2} ... -> selects (sel1) ... for the calculation
```
##### Optional flags:
```text
+n (i)                 -> calculates the MSD only up to i frames from the reference (unclear)
+dt (r)                -> specifies the time interval between the frames in ps
+o (filename)          -> overrides the default output filename
+screen                -> writes the output on standard output (unit=6)
+stderr                -> writes the output on standard error (unit=0)
```
<br>

### -dmap
calculates the density map in the system. 
##### Required flags (at least one):
```text
+s (l1) {l2} ...       -> selects (i1) ... for the calculation
+i (l1) {l2} ...       -> selects (l1) ... for the calculation
```
##### Optional flags:
```text
+x                       -> 1D density profile along x 
+y                       -> 1D density profile along y 
+z                       -> 1D density profile along z 
+xy                      -> 2D density profile in the xy plane
+xz                      -> 2D density profile in the xz plane
+yz                      -> 2D density profile in the yz plane
+3d                      -> 3D density profile of the whole cell
+dr (0.2)                -> specifies the size of the bins in the distribution
+n (100)                 -> specifies the number of bins in the distribution
+o (filename)            -> overrides the default output filename
+screen                  -> writes the output on standard output (unit=6)
+stderr                  -> writes the output on standard error (unit=0)
```
##### Special cases - a cube file will be produced:
```text
+mol                     -> 3D density profile around a selected molecule
  +sel (sel1) ...        -> selects the molecule
  +dr (5.0)              -> Specifies the size of the box around the molecule
  +n (100)               -> specifies the number of bins in the distribution
+box                     -> 3D density profile in a box
  +origin (r1) (r2) (r2) -> selects the origin of the box within calculate the density map
  +dr (5.0)              -> Specifies the size of the box
  +n (100)               -> specifies the number of bins in the distribution
```
##### Examples:
```text
gpta.x -i coord.pdb trajectory.0.dcd \
       -dmap +z +s O2 +n 1000 

gpta.x -i coord.pdb trajectory.0.dcd \
       -system 3 Ca CO3 H2O +rebuild \
       -pbc -fixcom +centre +s C4 \
       -select +s C4 O4 -align +sel 1 \
       -dmap +mol +sel 1 +s O2 +o dmap.cube
```
<br>

### -xray
calculates the powder diffraction spectrum for an xray wavelength of 1.54056 angstrom
##### Optional flags:
```text
+r (1.540560) -> changes the wavelength of the xrays
+n (30)       -> changes the number of K vectors used
+l (5) (50)   -> changes the range of 2theta explored
+o (filename) -> overrides the default output filename
+screen       -> writes the output on standard output (unit=6)
+stderr       -> writes the output on standard error (unit=0)
```
<br>

### -connect
calculates the connectivity between selected species in the system.
It requires a file for defining the required parameters.
##### Sample input file
```text
minsize 3
threshold 20
average 10000
charge 3
Ca  2
C4 -2
C5 -1
pairs 3
Ca C4 3.9
Ca C5 3.9
C5 C5 5.0
```

##### Optional flags:
```text
+f (filename) overrides the default input filename (connect.dat)
+dump-curr enables dumping out the coordinates of current clusters
+dump-dead enables dumping out the coordinates of dead clusters
```
<br>

### -dipole
calculates the system and molecular average dipole moment.
The atomic charges should be know before this command is used.
Optionally this command allows for calculating the IR spectrum, which is then written in a file named IR-spectrum.out 
It needs to be preceded by `-top` or `-systems` (??).
##### Optional flags:
```text
+o (filename) -> overrides the default output filename
+screen       -> writes the output on standard output (unit=6)
+stderr       -> writes the output on standard error (unit=0)
+mol          -> enables the calculation of the averge molecular dipole
+dt           -> sets the temperature for the calculation of the dielectric constant

+ir           -> calculates the IR spectrum - needs gpta to be compiled with the FFTW2
  +dt (0.001) -> time interval in ps between the frames to get the frequency of the IR spectrum in cm^-1
  +n (10000)  -> length of the dipole-dipole autocorrelation function 
              (a second integer could be specified for smoothing the output IR spectrum) 
```
<br>

### -plumed
enables the use of the PLUMED 2 routines to analyse a trajectory.
Requires PLUME2 to be compiled and the presence of PLUMED, see the compilation instructions.
##### Optional flags:
```text
+f (filename) -> overrides the default input filename
+o (filename) -> overrides the default output filename
```
<br>

### -dumpcv
writes a trajectory file that contains only the frames that meet the selection criteria.
The auxiliary file must contain the same number of lines as the number of frames in the input trajectory.
The auxiliary information could be the CVs written by PLUMED, the energy, or anything else
Comments are allowed in the auxiliary file using the # character.
##### Required flags:
```text
+f (filename) -> specifies the filename where the s are stored
+n [n]        -> number of CVs per frames
+vec [p1] ... -> sets the target value for the CVs
+dr [r1] ...  -> define the tolerance to identify which frames to print out
```
##### Optional flags:
```text
+o (filename) -> overrides the default output trajectory filename
```
##### Example:
```text
gpta.x -i coord.pdb trajectory.0.dcd   \
       -top -fixcom +s C4 +centre -pbc \
       -dumpcv +f COLVAR +n 1 +vec 3 +dr 0.01 +o tmp.pdb
```
<br>

### -find_sym
tries to determine the space group of a crystal.
##### Note: 
It requires the SPG library that can be found in the `extras` folder.
##### Optional flags:
```text
+r (r) -> defined the tolerance on the atomic positions (default r=0.0001)
```
<br>

### -test
Useful for debugging, developing purposes and one off analysis.
##### Available flags:
```text
+navg   
+f      
+o      
+stdout 
+stdout 
+stderr 
+append 
+s      
+i      
+sel    
+r      
+dn     
+dr     
+x0     
+pos    
+v      
+vec    
+l      
+limit  
+m      
+mtx    
+n      
+dt     
+avg   
+navg   
```
<br>
