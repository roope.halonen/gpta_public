# How to share gpta.tar
. Find the file ID
```bash
curl https://gitlab.com/api/v3/projects/181337/repository/tree?private_token=<provate token>
```

To download a file without log in:
```bash
curl https://gitlab.com/api/v3/projects/181337/repository/raw_blobs/<file ID>?private_token=<provate token> > gpta.tgz
```
