#include <signal.h>
#ifdef CRAY
#define TRUE 0xff
#define FALSE 0
extern "C" {
extern void csignal_( int* signum, sighandler_t handler)
{
  signal(*signum, handler);
}
}
#else
typedef void (*sighandler_t)(int);
void csignal_( int* signum, sighandler_t handler)
{
  signal(*signum, handler);
}
#endif
