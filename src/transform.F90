subroutine transform(comms)
  use variables, only : ip, dp, cp, command_type &
                      , natoms, pos, hmat, hinv, volume &
                      , nneigh, lneigh, rneigh

  use selection_module, only : atom_selection

  implicit none
  type(command_type), intent(in) :: comms
  integer(ip) :: i, indx

  integer(ip) :: nn
  integer(ip) :: iatm, jatm, ineigh, katm
  real(dp), allocatable, dimension (:,:) :: tmp_pos 
  real(dp), dimension(3) :: origin, axis
  real(dp) :: phi, rcut
  logical, allocatable, dimension(:) :: tmp_idx
  integer(ip) :: ntmp, ntmp0

#ifdef DEBUG
  write(0,*)"Entering subroutine :: transform"
#endif

  if ( comms%nsel>0_ip ) then
    nn = count(atom_selection==comms%sel(1))
    allocate(tmp_pos(3,nn))
    indx=0_ip
    do i=1,natoms
      if ( atom_selection(i) /= comms%sel(1) ) cycle
      indx = indx + 1_ip
      tmp_pos(1:3,indx) = pos(1:3,i)
    enddo
  else
    nn=natoms
    allocate(tmp_pos(3,nn))
    tmp_pos = pos
  endif

  do i=1,comms%nflags
! move atoms: requires +dr
    if (comms%flags(i) == "move") call translate(nn,tmp_pos,comms%dr(1:3))

! rotate atoms: requires +vec +r 
    if (comms%flags(i) == "rotate") then

      if (comms%ngidx==1) then
        iatm = comms%gidx(1,1)
        jatm = comms%gidx(2,1)
        origin(1:3) = pos(1:3,iatm)
        axis(1:3)   = pos(1:3,jatm)-pos(1:3,iatm)
        phi         = comms%real
      else
        origin(1:3) = comms%pos(1:3)
        axis(1:3)   = comms%vec(1:3)
        phi         = comms%real
      endif

      call rotate(nn,tmp_pos,origin,axis,phi)

      if (any(comms%flags=="torsion")) then
        if (comms%rneigh<0.1_dp) then
          rcut=2.0_dp
        else 
          rcut=comms%rneigh
        endif

        allocate(tmp_idx(natoms))
        tmp_idx=.true.
        do ineigh=1,nneigh(iatm)
          if (lneigh(ineigh,iatm)==jatm) cycle
          if (rneigh(ineigh,iatm)<rcut) then
            tmp_idx(lneigh(ineigh,iatm))=.false.
          endif
        enddo 
        ntmp0 = 0_ip
        ntmp  =count(tmp_idx)
        do while (ntmp /= ntmp0)
          ntmp0 = ntmp
          do iatm=1,natoms
          if (tmp_idx(iatm)) cycle
            do ineigh=1,nneigh(iatm)
              if (lneigh(ineigh,iatm)==jatm) cycle
              if (rneigh(ineigh,iatm)<rcut) then
                tmp_idx(lneigh(ineigh,iatm))=.false.
              endif
            enddo 
          enddo
          ntmp  =count(tmp_idx)
        enddo
        
  if ( comms%nsel>0_ip ) then
    indx=0_ip
    do iatm=1,natoms
      if ( atom_selection(iatm) /= comms%sel(1) ) cycle
      if (tmp_idx(iatm)) cycle
      indx = indx + 1_ip
      tmp_pos(1:3,indx) = pos(1:3,iatm)
    enddo
  else
    do iatm=1,natoms
      if (tmp_idx(iatm)) cycle
      tmp_pos(1:3,iatm) = pos(1:3,iatm)
    enddo
  endif




        deallocate(tmp_idx)
      endif

    endif

! cell scale
    if (comms%flags(i) == "scale") call cell_scale_1(nn,tmp_pos,comms%vec(1:3),comms%mtx,hmat,hinv,volume)

! mirror plane: requires +vec +pos
    if (comms%flags(i) == "mirror") call mirror(nn,tmp_pos,comms%pos,comms%vec(1:3),hmat,hinv)
  enddo

  if ( comms%nsel>0_ip ) then
    indx=0_ip
    do i=1,natoms
      if ( atom_selection(i) /= comms%sel(1) ) cycle
      indx = indx + 1_ip
      pos(1:3,i) = tmp_pos(1:3,indx)
    enddo
  else
    pos = tmp_pos
    deallocate(tmp_pos)
  endif
   
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: transform"
#endif

  return
end subroutine transform

subroutine cell_scale_1(natoms,pos,vec,mtx,hnew,hinv,vol)
  use variables, only : ip, dp, message
  implicit none
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(inout) :: pos
  real(dp), intent(in) :: vec(3), mtx(3,3)
  real(dp), dimension(3,3), intent(inout) :: hnew, hinv
  real(dp), intent(inout) :: vol

  integer(ip) :: i
  real(dp) :: sij(3)

#ifdef DEBUG
  write(0,*)"Entering subroutine :: cell_scale_1"
#endif

  if ( all(vec /= 0.0_dp) )then
    hnew(1:3,1)=vec(1)*hnew(1:3,1)
    hnew(1:3,2)=vec(2)*hnew(1:3,2)
    hnew(1:3,3)=vec(3)*hnew(1:3,3)
  elseif ( mtx(1,1)>0.0_dp .and. mtx(2,2)>0.0_dp .and. mtx(3,3)>0.0_dp ) then
    hnew(1:3,1)=mtx(1:3,1)
    hnew(1:3,2)=mtx(1:3,2)
    hnew(1:3,3)=mtx(1:3,3)
  else
    call message(0_ip,0_ip,0_ip,"No cell scaling defined")
  endif

  do i=1,natoms
    sij(1) = hinv(1,1)*pos(1,i) + hinv(1,2)*pos(2,i) + hinv(1,3)*pos(3,i)
    sij(2) = hinv(2,1)*pos(1,i) + hinv(2,2)*pos(2,i) + hinv(2,3)*pos(3,i)
    sij(3) = hinv(3,1)*pos(1,i) + hinv(3,2)*pos(2,i) + hinv(3,3)*pos(3,i)

    pos(1,i) = hnew(1,1)*sij(1) + hnew(1,2)*sij(2) + hnew(1,3)*sij(3)
    pos(2,i) = hnew(2,1)*sij(1) + hnew(2,2)*sij(2) + hnew(2,3)*sij(3)
    pos(3,i) = hnew(3,1)*sij(1) + hnew(3,2)*sij(2) + hnew(3,3)*sij(3)
  enddo

  call get_3x3_inv (hnew,hinv,vol)

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: cell_scale_1"
#endif

  return
end subroutine cell_scale_1

subroutine translate(natoms,pos,dr)                  
  use variables, only : ip, dp

  implicit none
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(inout) :: pos
  real(dp), intent(in) :: dr(3)

  integer(ip) :: i                                

#ifdef DEBUG
  write(0,*)"Entering subroutine :: translate"
#endif
        
  do i=1,natoms
    pos(1:3,i) = pos(1:3,i) + dr(1:3)
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: translate"
#endif

  return
end subroutine translate

subroutine  rotate(natoms,pos,org,vec,theta)
  use variables, only : ip, dp, pi
  implicit none
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(inout) :: pos
  real(dp), intent(in) :: org(3)
  real(dp), intent(in) :: vec(3), theta

  integer(ip) :: i
  real(dp) :: v(3), v2(3), costheta, sintheta, u(3), rcom(3)
  real(dp), dimension (3,3) :: rotmat
  real(dp), dimension (0:3) :: q
  real(dp) :: norm, theta2

#ifdef DEBUG
  write(0,*)"Entering subroutine :: rotate"
#endif

  norm=sqrt(sum(vec*vec))
  v=vec/norm
  v2=v*v
  theta2=pi*theta/180.0_dp
  costheta=cos(theta2)
  sintheta=sin(theta2)

  rotmat(1,1)=v2(1)+(1.-v2(1))*costheta
  rotmat(1,2)= v(1)*v(2)*(1-costheta)-v(3)*sintheta
  rotmat(1,3)= v(1)*v(3)*(1-costheta)+v(2)*sintheta

  rotmat(2,1)= v(1)*v(2)*(1-costheta)+v(3)*sintheta
  rotmat(2,2)=v2(2)+(1.-v2(2))*costheta
  rotmat(2,3)= v(2)*v(3)*(1-costheta)-v(1)*sintheta

  rotmat(3,1)= v(1)*v(3)*(1-costheta)-v(2)*sintheta
  rotmat(3,2)= v(2)*v(3)*(1-costheta)+v(1)*sintheta
  rotmat(3,3)=v2(3)+(1.-v2(3))*costheta

!  rcom=0.0_dp
!  do i=1,natoms
!    rcom=rcom+pos(1:3,i)-org
!  enddo
!  rcom=rcom/real(natoms,dp)
!  do i=1,natoms
!    u=pos(1:3,i)-rcom(1:3)
!    pos(1:3,i)=matmul(rotmat,u(1:3))+rcom(1:3)
!  enddo

  do i=1,natoms
    u=pos(1:3,i)-org(1:3)
    pos(1:3,i)=matmul(rotmat,u(1:3))+org(1:3)
write(123,*)i,u,pos(1:3,i)
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: rotate"
#endif

  return
end subroutine rotate

subroutine mirror(natoms,pos,org,vec,hmat,hinv)
  use variables, only : ip, dp

  implicit none
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(inout) :: pos
  real(dp), intent(in) :: org(3)
  real(dp), intent(in) :: vec(3)
  real(dp), intent(in) :: hmat(3,3)
  real(dp), intent(in) :: hinv(3,3)

  integer(ip) :: i
  real(dp), dimension(3) :: norm, dij, sij
  real(dp) :: rtmp

#ifdef DEBUG
  write(0,*)"Entering subroutine :: mirror"
#endif

  rtmp = sqrt(sum(vec*vec))
  norm = vec/rtmp

  do i=1,natoms
    dij(1:3) = pos(1:3,i) - org(1:3)

    rtmp = 2.0_dp * dot_product(norm,dij)

    pos(1:3,i) = pos(1:3,i) - rtmp*norm
  enddo

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: mirror"
#endif

  return
end subroutine mirror

