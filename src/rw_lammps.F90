!______________________________________________________________________________________________!
! LAMMPS
subroutine get_natom_lmp(uinp,natoms)
  use variables, only : ip, message
  implicit none
  integer(ip) :: uinp, natoms
  character(len=100) :: dummy1, dummy2
  logical :: look

#ifdef DEBUG
  write(0,*)"Entering subroutine :: get_natom_lmp"
#endif

  look=.true.
  do while (look)
    read(uinp,*,end=225,err=224)dummy1,dummy2
    if(dummy2=="atoms" .or. dummy2=="ATOMS" .or. dummy2=="Atoms")then
      look=.false.
      read(dummy1,*)natoms
    endif
224   continue
  enddo
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: get_natom_lmp"
#endif
  return
225 call message(0_ip,0_ip,0_ip,"Cannot find the number of atoms in the lammps file")
  stop
end subroutine get_natom_lmp

subroutine get_natom_lmptrj(uinp,natoms)
  use variables, only : ip, message
  implicit none
  integer(ip) :: uinp, natoms
  character(len=100) :: line
  logical :: look

#ifdef DEBUG
  write(0,*)"Entering subroutine :: get_natom_lmptrj"
#endif

  look=.true.
  do while (look)
    read(uinp,'(a100)',end=225,err=224)line
    if(trim(line) == "ITEM: NUMBER OF ATOMS") then
      read(uinp,*)natoms
      look=.false.
    endif
224   continue
  enddo
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: get_natom_lmptrj"
#endif
  return
225 call message(0_ip,0_ip,0_ip,"Cannot find the number of atoms in the lammps trajectory file")
  stop
end subroutine get_natom_lmptrj

subroutine read_lmp(uinp,natoms,pos,label,h,go)
  use variables, only: ip, dp, cp, message, ldo_cluster
  use elements_module, only : mass_to_label
  implicit none
  integer(ip), intent(in) :: uinp
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(out)  :: pos
  character(cp), dimension(natoms), intent(out)  :: label
  real(dp), intent(out)  :: h(3,3)
  logical, intent(out)  :: go

  character(len=180)  :: line
  character(len=20)  :: str1, str2, str3, str4, str5, str6
  integer(ip) :: nw, itmp, jj, kk, iatom, jatm
  real(dp) :: rtmp1, rtmp2
  integer(ip) :: ntyp, nn
  character(cp), dimension(:), allocatable :: itype
  real(dp), dimension(:), allocatable :: rmass
  logical, save :: first_time_in = .true.

#ifdef DEBUG
  write(0,*)"Entering subroutine :: read_lmp"
#endif

  go=.false.
  if (first_time_in) then
    first_time_in=.false.
  else
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmp"
#endif
    return
  endif

  do 
    read(uinp,'(a180)',end=227)line
    call count_words(line,len(line),nw)

! atoms types
    if(nw==3)then
      read(line,*)str1,str2,str3
      if(str2=="atom" .and. str3=="types")then
        read(str1,*)ntyp
        allocate(itype(ntyp))
        allocate(rmass(ntyp))
        do iatom=1,ntyp
          write(itype(iatom),'(i2)')iatom
        enddo
      endif
    endif

! diagonal element of the cell
    if(nw==4)then
      ldo_cluster = .false.
      read(line,*)str1,str2,str3,str4
      if    (str3=="xlo" .and. str4=="xhi")then
        read(line,*)rtmp1,rtmp2
        h(1,1)=rtmp2-rtmp1
      elseif(str3=="ylo" .and. str4=="yhi")then
        read(line,*)rtmp1,rtmp2
        h(2,2)=rtmp2-rtmp1
      elseif(str3=="zlo" .and. str4=="zhi")then
        read(line,*)rtmp1,rtmp2
        h(3,3)=rtmp2-rtmp1
      endif
    endif

! off diagonal element of the cell
    if(nw==6)then
      read(line,*)str1,str2,str3,str4,str5,str6
      if(str4=="xy" .and. str5=="xz" .and. str6=="yz")read(line,*)h(1,2),h(1,3),h(2,3)
    endif

    if(nw==1)then
      read(line,*)str1

! masses
      if(str1=="Masses")then
        iatom=0
        rmass=0.0_dp
        do while (iatom<ntyp)
          read(uinp,'(a180)')line
          if(len(trim(line))==0)cycle
          iatom=iatom+1
          read(line,*)itmp,rmass(itmp)
          itype(itmp) = mass_to_label(rmass(itmp))
          nn=0
          do jj=1,ntyp !iatom-1
            if (jj==itmp) cycle
            if(abs(rmass(itmp)-rmass(jj))<0.1)nn=nn+1
          enddo
          if(nn>0)then
            nw=len(trim(itype(itmp)))+1
            write(itype(itmp)(nw:nw),'(i1)')nn
          endif
        enddo

! atoms' coordinates
      elseif(str1=="Atoms")then
        iatom=0
        do while (iatom<natoms)
          read(uinp,'(a180)')line
          if(len_trim(line)==0)cycle
          iatom=iatom+1
          read(line,*,err=227,end=227)jatm, itmp, itmp, rtmp1, pos(1:3,iatom)
          label(jatm)=itype(itmp)
          cycle
          iatom=iatom-1
        enddo
        go=.true.
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmp"
#endif
        return

      endif
    endif
  enddo
227 continue
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmp"
#endif
  return
end subroutine read_lmp

subroutine read_lmptrj(uinp,natoms,pos,label,charge,h,go)
  use variables, only: ip, dp, cp, message, ldo_cluster
  use elements_module, only : mass_to_label
  implicit none
  integer(ip), intent(in) :: uinp
  integer(ip), intent(in) :: natoms
  real(dp), dimension(3,natoms), intent(out)  :: pos
  character(cp), dimension(natoms), intent(out)  :: label
  real(dp), dimension(natoms), intent(out)  :: charge
  real(dp), intent(out)  :: h(3,3)
  logical, intent(out)  :: go

  integer(ip) :: i, nw, n0, iatm, itype
  real(dp) :: r0, r1, sij(3)
  character(len=80) :: line
  character(len=20) :: words
  integer(ip), save :: nat
  logical, save :: first_time_in=.true.

  real(dp) :: xlo_bound, xlo
  real(dp) :: xhi_bound, xhi
  real(dp) :: ylo_bound, ylo
  real(dp) :: yhi_bound, yhi
  real(dp) :: zlo_bound, zlo
  real(dp) :: zhi_bound, zhi
  real(dp) :: xy
  real(dp) :: xz
  real(dp) :: yz 
  real(dp) :: mass

#ifdef DEBUG
  write(0,*)"Entering subroutine :: read_lmptrj"
#endif

  if (first_time_in) then
    nat=natoms
    first_time_in=.false.
  endif

  charge=0.0_dp
  go=.false.
  do 
    read(uinp,'(a80)',end=228,err=228)line
    call count_words(line,len(line),nw)

! number of atoms
    if (trim(line) == "ITEM: NUMBER OF ATOMS") then
      read(uinp,*,end=228,err=228) n0
      if (n0<nat) then
        call message(1_ip,0_ip,0_ip,"WARNING : reduced number of atoms in the lammps trajectory file",n0)
        nat=n0
        pos=0.0_dp
      elseif (n0>nat) then
        call message(0_ip,0_ip,0_ip,"Increased number of atoms in the lammps trajectory file",n0)
      endif
    endif

! box
    if (line(1:25) == "ITEM: BOX BOUNDS pp pp pp") then
      h = 0.0_dp
      read(uinp,*,end=228,err=228)xlo, xhi
      read(uinp,*,end=228,err=228)ylo, yhi
      read(uinp,*,end=228,err=228)zlo, zhi
      h(1,1) = xhi-xlo
      h(2,2) = yhi-ylo
      h(3,3) = zhi-zlo
      ldo_cluster=.false.
    endif

! box
    if (line(1:25) == "ITEM: BOX BOUNDS xy xz yz") then
      h = 0.0_dp

      read(uinp,*,end=228,err=228)xlo_bound, xhi_bound, xy
      read(uinp,*,end=228,err=228)ylo_bound, yhi_bound, xz
      read(uinp,*,end=228,err=228)zlo_bound, zhi_bound, yz

      xlo = xlo_bound - MIN(0.0,xy,xz,xy+xz)
      xhi = xhi_bound - MAX(0.0,xy,xz,xy+xz)
      ylo = ylo_bound - MIN(0.0,yz)
      yhi = yhi_bound - MAX(0.0,yz)
      zlo = zlo_bound 
      zhi = zhi_bound  

      h(1,1) = xhi-xlo
      h(2,2) = yhi-ylo
      h(3,3) = zhi-zlo
      h(1,2) = xy
      h(1,3) = xz
      h(2,3) = yz
      ldo_cluster=.false.
    endif


! atoms
    if (trim(line) == "ITEM: ATOMS id type xs ys zs") then
      do i=1,nat
        read(uinp,*,end=228,err=228)iatm,label(iatm),sij(1:3)
        pos(1,iatm) = h(1,1)*sij(1) + h(1,2)*sij(2) + h(1,3)*sij(3)
        pos(2,iatm) = h(2,1)*sij(1) + h(2,2)*sij(2) + h(2,3)*sij(3)
        pos(3,iatm) = h(3,1)*sij(1) + h(3,2)*sij(2) + h(3,3)*sij(3)
      enddo
      go=.true.
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmptrj"
#endif
      return
    endif

! atoms
    if (trim(line) == "ITEM: ATOMS id type x y z") then
      do i=1,nat
        read(uinp,*,end=228,err=228)iatm,label(iatm),pos(1:3,iatm)
      enddo
      go=.true.
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmptrj"
#endif
      return
    endif

    if (line(1:28) == "ITEM: ATOMS id mass xu yu zu") then
      do i=1,nat
        read(uinp,*,end=228,err=228)iatm,mass,pos(1:3,iatm),charge(iatm)
        label(iatm) = mass_to_label(mass)
      enddo
      go=.true.
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmptrj"
#endif
      return
    endif

  enddo
228 continue
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: read_lmptrj"
#endif
  return
end subroutine read_lmptrj

