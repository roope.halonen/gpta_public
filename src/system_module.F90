module system_module

  use variables, only : ip, dp, cp, command_type, natoms, pos, charge, label, hmat, hinv &
                      , num_mol_kinds, num_mol_of_kind, nmols, mol_kind, nmols, molecule, molecule_type &
                      , message, nneigh, lneigh, rneigh, neighmax, iverbose, ldo_cluster
  implicit none

  integer(ip), allocatable, dimension(:) :: atm2mol
  real(dp), allocatable, dimension(:,:) :: pos_tmp
  character(cp), allocatable, dimension(:) :: label_tmp
  real(dp), allocatable, dimension(:) :: charge_tmp
  type(molecule_type), allocatable, dimension(:) :: molecule_tmp

contains

  subroutine define_system(comms,iframe)
    use elements_module, only : max_bond_len
    use neighbour_list, only : sort_neighbours
    implicit none

    type(command_type), intent(in) :: comms
    integer(ip), intent(in) :: iframe

    integer(ip) :: i, j
    integer(ip) :: imol, jmol, imk, itmp
    integer(ip) :: iatm, jatm, katm, icount, jcount, jcount0
    integer(ip) :: nat, nmm
    logical, allocatable, dimension(:) :: used_atm 
    integer(ip), save, allocatable, dimension(:) :: new_indx

    real(dp) :: rcontact, dij(3), dist

    real(dp) :: rscale_sys
    logical, save :: first_time_in=.true.

    integer(ip) :: stat

#ifdef DEBUG
    write(0,*)"Entering subroutine :: define_system"
#endif

! Reorder molecules and atoms
    if (first_time_in) then
      if (.not. allocated(atm2mol)) allocate(atm2mol(natoms))
      atm2mol = 0_ip
      allocate(new_indx(natoms))
    else
!      return
    endif

! Sort neighbours
    call sort_neighbours()

    allocate(pos_tmp(3,natoms)) ; pos_tmp(1:3,1:natoms) = pos(1:3,1:natoms)
    allocate(label_tmp(natoms)) ; label_tmp(1:natoms) = label(1:natoms)
    allocate(charge_tmp(natoms)) ; charge_tmp(1:natoms) = charge(1:natoms)

    if (.not. first_time_in) then
      do i=1,natoms
        iatm=new_indx(i)
        pos( 1:3,iatm) = pos_tmp( 1:3,i)
        label(   iatm) = label_tmp(   i)
        charge(iatm) = charge_tmp(i)
      enddo
      deallocate(pos_tmp)
      deallocate(label_tmp)
      deallocate(charge_tmp)
#ifdef DEBUG
  write(0,*)" Leaving subroutine :: define_system"
#endif
      return
    endif

    allocate(used_atm(natoms)) ; used_atm = .false.
    allocate(molecule_tmp(natoms))

    nmm = 0_ip
    rscale_sys = comms%real

! Looping over the user-defined molkinds
    do imk=1,num_mol_kinds

! Looking for the first atom in the molecule
    a:do iatm=1,natoms
        if (used_atm(iatm)) cycle
        if (mol_kind(imk)%label(1) /= label(iatm)) cycle

        nmm = nmm + 1_ip
        allocate(molecule_tmp(nmm)%list(mol_kind(imk)%natoms))
  
        molecule_tmp(nmm)%kind = imk
        molecule_tmp(nmm)%resname = mol_kind(imk)%resname
        molecule_tmp(nmm)%natoms = mol_kind(imk)%natoms
        molecule_tmp(nmm)%list(1) = iatm
        used_atm(iatm) = .true.
      
        jcount = 1_ip
        if (jcount == molecule_tmp(nmm)%natoms) cycle

! Looping over the atoms in the molecule I'm building
      b:do icount=1,molecule_tmp(nmm)%natoms
          katm = molecule_tmp(nmm)%list(icount)

          jcount0 = jcount
! Looping over the neighbours
          do itmp=1,nneigh(katm)
            jatm=lneigh(itmp,katm)
            if (used_atm(jatm)) cycle

            rcontact = max_bond_len(label(katm) , label(jatm) , rscale_sys)
            if (rcontact<1.e-6_dp) cycle
            dist = rneigh(itmp,katm)

! Found a covalently bonded atom
            if (dist < rcontact) then
              if (any(mol_kind(imk)%label(1:mol_kind(imk)%natoms) == label(jatm))) then
                used_atm(jatm) = .true.
                jcount = jcount + 1_ip
                molecule_tmp(nmm)%list(jcount) = jatm
                if (jcount == molecule_tmp(nmm)%natoms) exit b
              endif
            endif
          enddo

        enddo b
      enddo a

! End with an error to prevent a seg-fault
     if (jcount/=mol_kind(imk)%natoms) call message(0_ip,0_ip,0_ip,"Cannot assign all molecules of type",imk)

    enddo

    deallocate(used_atm)

! Reorder the molecules and the atoms
    nmols = nmm
    allocate(molecule(nmols))
    imk=0_ip
    imol=0_ip
    iatm=0_ip
    num_mol_of_kind=0_ip
    do while (imk<=num_mol_kinds)
      imk = imk + 1_ip
      do jmol=1,nmols
        if (imk /= molecule_tmp(jmol)%kind) cycle
        imol = imol + 1_ip
        num_mol_of_kind(imk) = num_mol_of_kind(imk) + 1_ip
        allocate(molecule(imol)%list(mol_kind(imk)%natoms))
        molecule(imol)%kind = imk
        molecule(imol)%natoms =  mol_kind(imk)%natoms
        do jatm=1,mol_kind(imk)%natoms

! Need to reorder the atom in the molecules
          do katm=1,mol_kind(imk)%natoms
            itmp = molecule_tmp(jmol)%list(katm)
            if (itmp == 0_ip) cycle
            if (label_tmp(itmp) == mol_kind(imk)%label(jatm)) then

              iatm = iatm + 1_ip
              atm2mol( iatm) = imol
              pos( 1:3,iatm) = pos_tmp( 1:3,itmp)
              label(   iatm) = label_tmp(   itmp)
              charge(iatm) = charge_tmp(itmp)
              new_indx(itmp) = iatm

              if (jatm == 1) molecule(imol)%first_atom = iatm
              molecule(imol)%list(jatm) = iatm
              molecule_tmp(jmol)%list(katm) = 0_ip
              exit
            endif
          enddo
        enddo
      enddo

    enddo

    call get_number_of_atoms_per_kind()

    deallocate(molecule_tmp)
    deallocate(pos_tmp)
    deallocate(label_tmp)
    deallocate(charge_tmp)

! Rebuild molecule if necessary
    if (any(comms%flags == "rebuild")) then
      if (ldo_cluster) then
        call message(1_ip,1_ip,1_ip,"Warning :: +rebuild ignored - no cell parameters")
      else 
        call rebuild_molecules()
      endif
    endif
    first_time_in=.false.

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: define_system"
#endif

    return
  end subroutine define_system

  subroutine define_system_notop(comms,iframe)
    implicit none

    type(command_type), intent(in) :: comms
    integer(ip), intent(in) :: iframe

    integer(ip) :: imol, jmol, imk
    integer(ip) :: iatm, jatm, katm

    integer(ip) :: i
    character(len=100) :: str
    logical, save :: first_time_in=.true.

#ifdef DEBUG
   write(0,*)"Entering subroutine :: define_system_notop"
#endif

    if (.not. first_time_in) return

    if (.not. allocated(atm2mol)) allocate(atm2mol(natoms))
    allocate(molecule_tmp(natoms))

    if (any(comms%flags == "reorder")) then
      allocate(pos_tmp(3,natoms)) ; pos_tmp(1:3,1:natoms) = pos(1:3,1:natoms)
      allocate(label_tmp(natoms)) ; label_tmp(1:natoms) = label(1:natoms)
      allocate(charge_tmp(natoms)) ; charge_tmp(1:natoms) = charge(1:natoms)
    endif 

    imol = 0_ip
    imk = 1_ip
    iatm = 0_ip
    num_mol_of_kind = 0_ip
    do while (iatm < natoms)
      do imk=1,num_mol_kinds
        jatm = mol_kind(imk)%natoms
        if (iatm+jatm>natoms) cycle
        if (jatm/=mol_kind(imk)%natoms) cycle
        if (all (label(iatm+1:iatm+jatm) == mol_kind(imk)%label(1:jatm)) ) exit
      enddo
      if (imk>num_mol_kinds) then
        call message(1_ip,0_ip,0_ip,"Cannot assign all the atoms to a molecule - atom",iatm+1_ip)
        call message(0_ip,0_ip,0_ip,"Cannot assign all the atoms to a molecule - atom",label(iatm+1))
      endif
      imol = imol + 1_ip
      num_mol_of_kind(imk) = num_mol_of_kind(imk) + 1_ip
      allocate(molecule_tmp(imol)%list(mol_kind(imk)%natoms))
      molecule_tmp(imol)%kind = imk
      molecule_tmp(imol)%resname = mol_kind(imk)%resname
      molecule_tmp(imol)%natoms = mol_kind(imk)%natoms
      do jatm=1,mol_kind(imk)%natoms
        iatm = iatm + 1_ip
        if (jatm == 1) molecule_tmp(imol)%first_atom = iatm
        molecule_tmp(imol)%list(jatm) = iatm
        atm2mol(iatm) = imol
      enddo
    enddo

    nmols = imol
    allocate(molecule(nmols))
!    if (first_time_in) allocate(molecule(nmols))

    if (any(comms%flags == "reorder")) then
      jmol=0_ip
      jatm=0_ip
      do imk=1,num_mol_kinds
        do imol=1,nmols
          if (molecule_tmp(imol)%kind == imk) then
            jmol = jmol + 1_ip
            if (first_time_in) allocate(molecule(jmol)%list(molecule_tmp(imol)%natoms))
            molecule(jmol)%kind = imk
            molecule(jmol)%resname = molecule_tmp(imol)%resname
            molecule(jmol)%first_atom = jatm + 1_ip
            molecule(jmol)%natoms = molecule_tmp(imol)%natoms
            do iatm=1,molecule_tmp(imol)%natoms
              jatm=jatm+1_ip
              molecule(jmol)%list(iatm) = jatm
              katm                      = molecule_tmp(imol)%list(iatm)
              pos( 1:3,jatm)            = pos_tmp( 1:3,katm)
              label(   jatm)            = label_tmp(   katm)
              charge(  jatm)            = charge_tmp(  katm)
              atm2mol( jatm)            = jmol
            enddo
          endif
        enddo
      enddo
    else
      do imol=1,nmols
        if (first_time_in) allocate(molecule(imol)%list(molecule_tmp(imol)%natoms))
        molecule(imol) = molecule_tmp(imol)
      enddo
    endif

    deallocate(molecule_tmp)
    if (any(comms%flags == "reorder")) then
      deallocate(pos_tmp)
      deallocate(label_tmp)
      deallocate(charge_tmp)
    endif 

    if (any(comms%flags == "rebuild")) then
      if (ldo_cluster) then
        call message(1_ip,1_ip,1_ip,"Warning :: +rebuild ignored - no cell parameters")
      else      
        call rebuild_molecules()
      endif
    endif

    if (iverbose>0 .and. iframe==1) then
      call message(1_ip,0_ip,1_ip," Define system - molecules found ",nmols)
      call message(1_ip,0_ip,1_ip,"                 types of molecules found ",num_mol_kinds)
      write(str,'(20(i6,1x))') (num_mol_of_kind(i),i=1,num_mol_kinds)
      call message(1_ip,1_ip,1_ip,"                 number of molecules for each type ",str)
    endif

    first_time_in=.false.

#ifdef DEBUG
  write(0,*)" Leaving subroutine :: define_system_notop"
#endif

    return
  end subroutine define_system_notop

end module system_module

