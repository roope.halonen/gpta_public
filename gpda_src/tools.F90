subroutine get_words(str,l,n,words)
   use variables, only : ip, MAXLEN
   implicit none

   integer(ip),       intent(in)  :: l
   character(len=l),  intent(in)  :: str
   integer(ip),       intent(out) :: n
   character(MAXLEN), intent(out) :: words(MAXLEN)
   integer(ip)                    :: ios
   do n=0,size(words)-1
     read(str,*,iostat=ios) words(1:n+1)
     if(ios/=0) return
   end do
   return
end subroutine get_words

subroutine reallocate_points(n1_old,n2_old,n1_new,n2_new)
  use variables, only : ip, dp, points
  implicit none

  integer(ip), intent(in) :: n1_old, n2_old, n1_new, n2_new

  real(dp), allocatable, dimension(:,:) :: tmp

  allocate(tmp(n1_old,n2_old))
  tmp(1:n1_old,1:n2_old) = points(1:n1_old,1:n2_old)
  deallocate(points)
  allocate(points(n1_new,n2_new))
  points(1:n1_old,1:n2_old) = tmp(1:n1_old,1:n2_old)
  deallocate(tmp)
  
  return
end subroutine reallocate_points

function calc_stats(comms) result(istat)
  use variables
  implicit none

  type(command_type), intent(in) :: comms
  integer(ip) :: istat

  real(dp) :: rmin, rmax
  real(dp) :: res
  real(dp) :: res2

  integer(ip) :: i, j, ic, ib, istart, iend, npts

  if (any(comms%flag=="all"))then
    nblocks=1
    pblock(0)=0_ip
    pblock(1)=ndat
  endif

  if (comms%nstart>0 .or. comms%nend>0) then
    nblocks=1
    if (comms%nstart>0) pblock(0)=comms%nstart-1_ip
    if (comms%nend>0) pblock(1)=comms%nend
  endif

  do j=1,ncols
    if (comms%ncols>0)then
      if (any(comms%icol(1:comms%ncols)==icol(j))) then
        ic=j
      else
        cycle
      endif
    else
      ic=j
    endif
    do ib=1,nblocks
      if (comms%nblocks==0 .or. any(comms%iblock==ib)) then
        istart = pblock(ib-1)+1_ip
        iend = min(pblock(ib),ndat)
        if (istart > iend) exit

! Minimum
        rmin=points(istart,ic)
        do i=istart,iend,comms%nskip
          rmin = min(points(i,ic),rmin)
        enddo

! Maximum
        rmax=points(istart,ic)
        do i=istart,iend,comms%nskip
          rmax = max(points(i,ic),rmax)
        enddo

! Average and Standard deviation
        npts=0_ip
        res=0.0_dp
        res2=0.0_dp
        do i=istart,iend,comms%nskip
          res  = res  + points(i,ic)
          res2 = res2 + points(i,ic)**2
          npts = npts + 1_ip
        enddo

        res = res / real(npts,dp)
        res2 = res2 / real(npts,dp)
      
        call message(1_ip,0_ip,1_ip,"Results for column",icol(ic))
        call message(1_ip,0_ip,1_ip,"  Block",ib)
        call message(1_ip,0_ip,1_ip,"    Data points",(/istart,iend/))
        call message(1_ip,0_ip,1_ip,"    Stride     ",comms%nskip)
        call message(1_ip,0_ip,1_ip,"    Minimum",rmin)
        call message(1_ip,0_ip,1_ip,"    Maximum",rmax)
        call message(1_ip,0_ip,1_ip,"    Average",res)
        call message(1_ip,1_ip,1_ip,"    Standard deviation",sqrt(res2-res**2))
      endif
    enddo
  enddo

  return
end function calc_stats

function calc_dist(comms) result(istat)
  use variables
  implicit none

  type(command_type), intent(inout) :: comms
  integer(ip) :: istat

  integer(ip) :: ib, istart, iend, npts
  integer(ip) :: i, j, ic

  integer(ip) :: nn
  real(dp)    :: dr
  real(dp)    :: rmin, rmax
  integer(ip) :: itmp

  open(comms%outfile,file=comms%outfilename,status='unknown',form='formatted')
  nn = comms%nbin

  if (any(comms%flag=="all"))then
    nblocks=1
    pblock(0)=1_ip
    pblock(1)=ndat
  endif

  if (comms%nstart>0 .or. comms%nend>0) then
    nblocks=1
    if (comms%nstart>0) pblock(0)=comms%nstart-1_ip
    if (comms%nend>0) pblock(1)=comms%nend
  endif

  do j=1,ncols
    if (comms%ncols>0)then
      if (any(comms%icol(1:comms%ncols)==icol(j))) then
        ic=j
      else
        cycle
      endif
    else
      ic=j
    endif
    do ib=1,nblocks
      if (comms%nblocks==0 .or. any(comms%iblock==ib)) then
        istart = pblock(ib-1)+1_ip
        iend = min(pblock(ib),ndat)

        if (comms%lmts(1) == comms%lmts(2))then
          rmin=points(istart,ic)
          do i=istart,iend,comms%nskip
            rmin = min(points(i,ic),rmin)
          enddo

          rmax=points(istart,ic)
          do i=istart,iend,comms%nskip
            rmax = max(points(i,ic),rmax)
          enddo
        else
          rmin = comms%lmts(1)
          rmax = comms%lmts(2)
        endif
    
        if (comms%real<0.0_dp) then
          dr = (rmax-rmin) / real(nn,dp)
        else 
          dr = comms%real
        endif
    
        call message(1_ip,0_ip,1_ip,"Distribution for column",icol(ic))
        call message(1_ip,0_ip,1_ip,"  Block",ib)
        call message(1_ip,0_ip,1_ip,"    Data points",(/istart,iend/))
        call message(1_ip,0_ip,1_ip,"    Stride     ",comms%nskip)
        call message(1_ip,0_ip,1_ip,"    Minimum",rmin)
        call message(1_ip,0_ip,1_ip,"    Maximum",rmax)
        call message(1_ip,0_ip,1_ip,"    Number of bins",nn)
        call message(1_ip,1_ip,1_ip,"    Bin width",dr)
    
        allocate(comms%dist(0:nn))
        comms%dist=0.0_dp
    
        do i=istart,iend,comms%nskip
          itmp = nint((points(i,ic)-rmin)/dr)
          comms%dist(itmp) = comms%dist(itmp) + 1.0_dp 
        enddo
        comms%dist = comms%dist / real(ndat,dp) / dr
        
        do i=0,nn
          write(comms%outfile,'(2(g10.5,1x))')rmin+(i)*dr,comms%dist(i)
        enddo
        write(comms%outfile,*)
        write(comms%outfile,*)
    
        deallocate(comms%dist)
      endif
    enddo
  enddo
  close(comms%outfile)

  return
end function calc_dist

function calc_integral(comms) result(istat)
  use variables
  implicit none

  type(command_type), intent(inout) :: comms
  integer(ip) :: istat

  integer(ip) :: ib, istart, iend
  integer(ip) :: i, j, ic, ix, iy

  integer(ip) :: nn
  real(dp) :: dx, rint
  real(dp) :: dd

  open(comms%outfile,file=comms%outfilename,status='unknown',form='formatted')
  nn = comms%nbin

  if (any(comms%flag=="all"))then
    nblocks=1
    pblock(0)=1_ip
    pblock(1)=ndat
  endif

  if (comms%nstart>0 .or. comms%nend>0) then
    nblocks=1
    if (comms%nstart>0) pblock(0)=comms%nstart-1_ip
    if (comms%nend>0) pblock(1)=comms%nend
  endif

  if (comms%ncols==0) call message(0_ip,0_ip,0_ip,"2 columns must be specified for the integration")
  do j=1,ncols
    if (icol(j)==comms%icol(1)) ix = icol(j)
    if (icol(j)==comms%icol(2)) iy = icol(j)
  enddo

  do ib=1,nblocks
    if (comms%nblocks==0 .or. any(comms%iblock==ib)) then
      istart = pblock(ib-1)+1_ip
      iend = min(pblock(ib),ndat)

      call message(1_ip,0_ip,1_ip,"Integral for columns",icol(ic))
      call message(1_ip,0_ip,1_ip,"  Limits",comms%lmts(1:2))
      call message(1_ip,0_ip,1_ip,"  Block",ib)
      call message(1_ip,0_ip,1_ip,"    Data points",(/istart,iend/))
   
      rint=0.0_dp
      if (any(comms%flag=="spherical")) then
        do i=istart,iend-1,comms%nskip
          dx = points(i+1,ix) - points(i,ix)
          dd = 4.0_dp * pi * points(i,ix)**2 * dx
          rint = rint + 0.5d0*dx*(points(i+1,iy)+points(i,iy)) * dd
          write(comms%outfile,'(2(g10.5,1x))')points(i,ix)+0.5_dp*dx,rint
        enddo
      else
        do i=istart,iend-1,comms%nskip
          if (points(i,ix)<comms%lmts(1) .or. points(i+1,ix)>comms%lmts(2)) cycle
          dx = points(i+1,ix) - points(i,ix)
          rint = rint + 0.5d0*dx*(points(i+1,iy)+points(i,iy))
          write(comms%outfile,'(2(e12.5,1x))')points(i,ix)+0.5_dp*dx,rint
        enddo
      endif
      
      write(comms%outfile,*)
      write(comms%outfile,*)
  
    endif
  enddo
  close(comms%outfile)

  return
end function calc_integral
