!
! Author          :: Paolo Raiteri
! Institution     :: Curtin University
! Email           :: p.raiteri@curtin.edu.au
!
module variables
  implicit none
!
! Precision
! 
! cp = character length
! ip = integer precision
! dp = floating point precision
!
  integer, parameter :: cp =  4
  integer, parameter :: ft =  6  ! file type
  integer, parameter :: ip  = 4  ! double precision
#ifdef SINGLE
  integer, parameter :: dp =  4  ! double precision
#else
  integer, parameter :: dp =  8  ! single precision
#endif
  real(dp), parameter :: pi    = 3.1415926535898_dp
  real(dp), parameter :: pih   = 1.5707963267949_dp
  real(dp), parameter :: twopi = 6.2831853071796_dp
  real(dp), parameter :: sqrpi = 1.772453850905515_dp
!
  integer(ip) :: io = 6
!
  integer(ip), parameter :: MAXBLOCKS=1000
  integer(ip), parameter :: MAXPARM=100
  integer(ip), parameter :: MAXPROP=100
  integer(ip), parameter :: MAXLEN=300
  integer(ip), parameter :: OUTLEN=100
  character(len=MAXLEN)  :: fmtstring0='(2x,a68)'
  character(len=MAXLEN)  :: fmtstring='(2x,a50," : ",a)'
!
  integer(ip) :: ndat
  integer(ip) :: nblocks = 1_ip
  integer(ip), dimension(0:MAXBLOCKS) :: pblock
  real(dp), allocatable, dimension(:,:) :: points

  integer(ip) :: ncols = 1_ip
  integer(ip), dimension(MAXPARM) :: icol = 1_ip
!
  integer(ip)                                 :: iounit = 200_ip
!
  type command_type
    character(len=10)                         :: type        = "NONE"
    character(len=10)                         :: subtype     = "NONE"
    integer(ip)                               :: inpfile     = 0_ip
    character(len=MAXLEN)                     :: inpfilename = "NONE"
    character(len=ft)                         :: inpfiletype = "NONE"
    integer(ip)                               :: outfile     = 0_ip
    character(len=MAXLEN)                     :: outfilename = "NONE"
    character(len=6)                          :: posfile     = "rewind"

    integer(ip)                               :: nskip       = 1_ip
    integer(ip)                               :: nstart      = 0_ip
    integer(ip)                               :: nend        = 0_ip
    integer(ip)                               :: nblocks     = 0_ip
    integer(ip), dimension(MAXPARM)           :: iblock      = 0_ip

    integer(ip)                               :: ncols       = 0_ip
    integer(ip), dimension(MAXPARM)           :: icol        = 0_ip

    integer(ip)                               :: nbin        = 50_ip
    real(dp)                                  :: real        = -1.0_dp
    real(dp), dimension(2)                    :: lmts        = -1.0_dp
    real(dp), allocatable, dimension(:)       :: dist

    integer(ip)                               :: nflags      = 0_ip
    character(len=MAXLEN), dimension(MAXPARM) :: flag        = ""

    integer(2)                                :: debug       = 0_ip
  end type command_type

  integer(ip) :: idebug=0
  integer(ip) :: iverbose=1
  integer(ip) :: iseed=4357

  public :: message, debug
  interface message
    module procedure s_message, i_message, iv_message, r_message, v_message, vs_message
  end interface message

  interface debug
    module procedure r_debug, rv_debug, m_debug, i_debug, iv_debug, s_debug
  end interface debug


contains
! ________________________________________________________________________________ !
! Print comment message
  subroutine printout(iterminate,istart,iend,string)
    implicit none
    integer(ip)     , intent(in) :: iterminate
    integer(ip)     , intent(in) :: istart
    integer(ip)     , intent(in) :: iend
    character(len=MAXLEN), intent(in) :: string
    logical, save                :: first=.true.
    integer           :: info(8)
    character(len=6) :: cprec
    character(len=100) :: cwd
! ________________________________________________________________________________ ! 
! Incipit 
    if (first .and. iverbose>0) then
      call date_and_time(values=info)
      call getcwd(cwd)
      write(io,'(90("-"))')
#ifdef SINGLE
      cprec = 'SINGLE'
#else
      cprec = 'DOUBLE'
#endif
      write(io,'("  GPDA Vesion 2.0 ",a6," PRECISION")')cprec

      write(io,'("   Started at ",i2.2,":",i2.2,":",i2.2," on ",i2.2,"/",i2.2,"/",i4," in")')&
        info(5),info(6),info(7),info(3),info(2),info(1)
      write(io,'("   ",a)')trim(cwd)

      write(io,'(90("-"))')
    endif
    first=.false.

    if (iterminate==0) then
      write(io,'("---- GPDA ENDED WITH AN ERROR ----",/,a)')trim(string)
      stop
    endif

    write(io,'(a)')trim(string)
    if (istart == iend) then
      write(io,'(90("-"))')
    endif

    if (iterminate<0_ip) stop

    return
  end subroutine printout
! ________________________________________________________________________________ !
! Comment message - Vector Real
  subroutine v_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip), intent(in) :: iterminate, istart, iend
    character(len=*), intent(in) :: msg
    real(dp), dimension(:), intent(in) :: var
    character(len=OUTLEN) :: dum, nr
    character(len=MAXLEN) :: string
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    write(nr,'(3(f10.5,1x))') var
    string = ""
    write (string,trim(fmtstring)) dum , trim(nr)
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine v_message
! ________________________________________________________________________________ !
! Comment message - Scalar Real
  subroutine r_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip)     , intent(in) :: iterminate, istart, iend
    character(len=*), intent(in) :: msg
    real(dp)        , intent(in) :: var
    character(len=OUTLEN)        :: dum, nr
    character(len=MAXLEN)        :: string
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    write(nr,'(f0.5)') var
    string = ""
    write (string,trim(fmtstring)) dum , adjustl(trim(nr))
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine r_message
! ________________________________________________________________________________ !
! Comment message - Vector integer
  subroutine iv_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip), intent(in) :: iterminate, istart, iend
    character(len=*), intent(in) :: msg
    integer(ip), dimension(:) , intent(in) :: var
    character(len=OUTLEN) :: dum, nr
    character(len=MAXLEN) :: string
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    write(nr,'(3(i10,1x))') var
    string = ""
    write (string,trim(fmtstring)) dum , adjustl(trim(nr))
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine iv_message
! ________________________________________________________________________________ !
! Comment message - Scalar integer
  subroutine i_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip)     , intent(in) :: iterminate, istart, iend
    character(len=*), intent(in) :: msg
    integer(ip)     , intent(in) :: var
    character(len=OUTLEN)        :: dum, nr
    character(len=MAXLEN)        :: string
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    write(nr,'(i0)') var
    string = ""
    write (string,trim(fmtstring)) dum , adjustl(trim(nr))
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine i_message
! ________________________________________________________________________________ !
! Comment message - Character
  subroutine s_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip)     , intent(in)           :: iterminate, istart, iend
    character(len=*), intent(in)           :: msg
    character(len=*), intent(in), optional :: var
    character(len=OUTLEN)                  :: dum
    character(len=MAXLEN)                  :: string
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    string = ""
    if (present(var))then
      write (string,'('//trim(fmtstring)//',a)') dum , adjustl(trim(var))
    else
      write (string,fmtstring0) dum
    endif
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine s_message
! ________________________________________________________________________________ !
! Comment message - Vector Character
  subroutine vs_message(iterminate,istart,iend,msg,var)
    implicit none
    integer(ip)     , intent(in)               :: iterminate, istart, iend
    character(len=*), intent(in)               :: msg
    character(len=*), intent(in), dimension(:) :: var
    character(len=OUTLEN)                      :: dum
    character(len=MAXLEN)                      :: string
    character(len=100)                         :: fmtstr
    dum    = ""
    if (istart<=1) then
      write(dum,'(a)')trim(msg)
    endif
    string = ""
    write(fmtstr,'(","i0,"a")')size(var)
    write (string,'('//trim(fmtstring)//trim(fmtstr)//')') dum , adjustl(var)
    call printout(iterminate,istart,iend,string)   
    return
  end subroutine vs_message

! Debug output
  subroutine r_debug(vec,msg)
    implicit none
    real(dp), intent(in) :: vec
    character(len=*), intent(in), optional :: msg
    integer(ip) :: nn
    character(len=100) :: str
    write(str,*)'((f10.5,1x))'
    if (present(msg)) write(0,'(a)')trim(msg)
    write(0,str) vec
    return
  end subroutine r_debug

  subroutine rv_debug(vec,msg)
    implicit none
    real(dp), dimension(:), intent(in) :: vec
    character(len=*), intent(in), optional :: msg
    integer(ip) :: nn
    character(len=100) :: str
    nn=size(vec)
    write(str,*)'(',nn,'(f10.5,1x))'
    if (present(msg)) write(0,'(a)')trim(msg)
    write(0,str) vec
    return
  end subroutine rv_debug

  subroutine m_debug(vec,msg)
    implicit none
    real(dp), dimension(:,:), intent(in) :: vec
    character(len=*), intent(in), optional :: msg
    integer(ip) :: nn, mm
    character(len=100) :: str
    integer(ip) :: i
    mm=size(vec(1,:))
    nn=size(vec(:,1))
    write(str,*)'(',nn,'(f10.5,1x))'
    if (present(msg)) write(0,'(a)')trim(msg)
    do i=1,mm
      write(0,str) vec(:,i)
    enddo
    return
  end subroutine m_debug

  subroutine i_debug(vec,msg)
    implicit none
    integer(ip), intent(in) :: vec
    character(len=*), intent(in), optional :: msg
    integer(ip) :: nn
    character(len=100) :: str
    if (present(msg)) write(0,'(a)')trim(msg)
    write(str,*)'((i10,1x))'
    write(0,str) vec
    return
  end subroutine i_debug

  subroutine iv_debug(vec,msg)
    implicit none
    integer(ip), dimension(:), intent(in) :: vec
    character(len=*), intent(in), optional :: msg
    integer(ip) :: nn
    character(len=100) :: str
    nn=size(vec)
    if (present(msg)) write(0,'(a)')trim(msg)
    write(str,*)'(',nn,'(i10,1x))'
    write(0,str) vec
    return
  end subroutine iv_debug

  subroutine s_debug(str)
    implicit none
    character(len=*), intent(in) :: str
    write(0,'(a)')trim(str)
    return
  end subroutine s_debug

end module variables
